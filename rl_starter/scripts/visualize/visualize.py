#!/usr/bin/env python3
# coding --utf-8--

from rl_starter.scripts.visualize import args
from array2gif import write_gif
import rl_starter.utils
import torch
import numpy
import time
import cv2


class Visualize(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            print('Creating the object...')
            cls._instance = super(Visualize, cls).__new__(cls)
            print('Done.')
            # Put any initialization here.
        return cls._instance

    def __set_device(self):
        rl_starter.utils.seed(self.args.seed)

        # Set device

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print(f"Device: {self.device}\n")

    def __load_env(self):
        self.env = rl_starter.utils.make_env(self.args.env, self.args.seed)

        for _ in range(self.args.shift):
            self.env.reset()

        print("Environment loaded\n")

    def __load_agent(self):
        self.model_dir = rl_starter.utils.get_model_dir(self.args.model)
        self.agent = rl_starter.utils.Agent(self.env.observation_space, self.env.action_space, self.model_dir,
                                            device=self.device, argmax=self.args.argmax, use_memory=self.args.memory,
                                            use_text=self.args.text)
        print("Agent loaded\n")

    def __init_visualize_part(self):
        self.frames = []

        # Create a window to view the environment
        # env.render('human')

        self.path_video = None
        self.video_writer = None

    def __init__(self):
        self.args = args.parse_visualize_args()

        self.__set_device()
        self.__load_env()
        self.__load_agent()

        self.__init_visualize_part()

    def __update_video_part(self):
        if self.args.gif or self.path_video is not None:
            frame = self.env.render("rgb_array")
            if self.args.gif:
                self.frames.append(numpy.moveaxis(frame, 2, 0))
            if self.path_video is not None:
                if self.video_writer is None:
                    self.video_writer = cv2.VideoWriter(self.path_video, cv2.VideoWriter_fourcc(*'XVID'), 10,
                                                        (frame.shape[1], frame.shape[0]))
                self.video_writer.write(frame)

    def __run_episode(self):
        obs = self.env.reset()

        reward = 0
        done = False
        while True:
            self.env.render('human')

            key = cv2.waitKey(0)
            if key == ord('q'):
                break

            self.__update_video_part()
            if done:
                print(f'Done. Reward: {reward}')
                break

            action = self.agent.get_action(obs)
            print(f'\tAction: {self.env.actions(action).name}')
            obs, reward, done, _ = self.env.step(action)
            print(f'\tReward for move: {reward}')
            self.agent.analyze_feedback(reward, done)
            time.sleep(0.1)

    def __save_gif_if_need(self):
        if self.args.gif:
            print("Saving gif... ", end="")
            write_gif(numpy.array(self.frames), self.args.gif + ".gif", fps=1 / self.args.pause)
            print("Done.")

    def __call__(self, *args, **kwargs):
        for episode in range(self.args.episodes):
            self.__run_episode()
            # if self.env.window.closed:
                # break
                
        self.__save_gif_if_need()
