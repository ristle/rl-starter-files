#!/usr/bin/env python3
# coding --utf-8--

from rl_starter.scripts import train

if __name__ == '__main__':
    train_attribute = train.Train()

    train_attribute.train_model()
