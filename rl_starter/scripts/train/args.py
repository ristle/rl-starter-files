#!/usr/bin/env python3
# coding --utf-8--

import argparse


def __parse_general_params(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    parser.add_argument("--algo", required=False, default='ppo',
                        help="algorithm to use: a2c | ppo (REQUIRED)")
    parser.add_argument("--env", required=False, default='ComplexGridWithAIRobots-v2',
                        help="name of the environment to train on (REQUIRED)")
    parser.add_argument("--model", required=False, default='ComplexGridWithAIRobots-v2-8',
                        help="name of the model (default: {ENV}_{ALGO}_{TIME})")
    parser.add_argument("--seed", type=int, default=1,
                        help="random seed (default: 1)")
    parser.add_argument("--log-interval", type=int, default=1,
                        help="number of updates between two logs (default: 1)")
    parser.add_argument("--save-interval", type=int, default=10,
                        help="number of updates between two saves (default: 10, 0 means no saving)")
    parser.add_argument("--procs", type=int, default=16,
                        help="number of processes (default: 16)")
    parser.add_argument("--frames", type=int, default=10 ** 7,
                        help="number of frames of training (default: 1e7)")
    parser.add_argument("--cpu", type=bool, default=False,
                        help="Turn cpu mode if it needed")
    parser.add_argument("--number_threads", type=int, default=16,
                        help="If CPU is enabled, try to use them!")
    return parser


def __parse_training_params(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    # Parameters for main algorithm
    parser.add_argument("--epochs", type=int, default=4,
                        help="number of epochs for PPO (default: 4)")
    parser.add_argument("--batch-size", type=int, default=256,
                        help="batch size for PPO (default: 256)")
    parser.add_argument("--frames-per-proc", type=int, default=None,
                        help="number of frames per process before update (default: 5 for A2C and 128 for PPO)")
    parser.add_argument("--discount", type=float, default=0.99,
                        help="discount factor (default: 0.99)")
    parser.add_argument("--lr", type=float, default=0.001,
                        help="learning rate (default: 0.001)")
    parser.add_argument("--gae-lambda", type=float, default=0.95,
                        help="lambda coefficient in GAE formula (default: 0.95, 1 means no gae)")
    parser.add_argument("--entropy-coef", type=float, default=0.01,
                        help="entropy term coefficient (default: 0.01)")
    parser.add_argument("--value-loss-coef", type=float, default=0.5,
                        help="value loss term coefficient (default: 0.5)")
    parser.add_argument("--max-grad-norm", type=float, default=0.5,
                        help="maximum norm of gradient (default: 0.5)")
    parser.add_argument("--optim-eps", type=float, default=1e-8,
                        help="Adam and RMSprop optimizer epsilon (default: 1e-8)")
    parser.add_argument("--optim-alpha", type=float, default=0.99,
                        help="RMSprop optimizer alpha (default: 0.99)")
    parser.add_argument("--clip-eps", type=float, default=0.2,
                        help="clipping epsilon for PPO (default: 0.2)")
    parser.add_argument("--recurrence", type=int, default=1,
                        help="number of time-steps gradient is backpropagated (default: 1)." + \
                             " If > 1, a LSTM is added to the model to have memory.")
    parser.add_argument("--text", action="store_true", default=False,
                        help="add a GRU to the model to handle text input")

    return parser


def parse_arguments_for_training_one_model():
    """
        Parse all needed helper function for training only one model
    """
    # Parse arguments
    parser = argparse.ArgumentParser()

    # General parameters
    parser = __parse_general_params(parser)
    parser = __parse_training_params(parser)

    args = parser.parse_args()
    args.mem = args.recurrence > 1

    return args
