#!/usr/bin/env python3
# coding --utf-8--

import sys
import time
import torch
import datetime
import torch_ac
import rl_starter.utils
from rl_starter.scripts import train
from rl_starter.model import ACModel
from torch.utils.tensorboard import SummaryWriter


class Train(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            print('Creating the object...')
            cls._instance = super(Train, cls).__new__(cls)
            print('Done.')
            # Put any initialization here.
        return cls._instance

    def __get_general_info(self):
        self.date = datetime.datetime.now().strftime("%y-%m-%d-%H-%M-%S")
        self.default_model_name = f"{self.args.env}_{self.args.algo}_seed{self.args.seed}"

        self.model_name = self.args.model or self.default_model_name
        self.model_dir = rl_starter.utils.get_model_dir(self.model_name)

        # Load loggers and Tensorboard writer

        self.txt_logger = rl_starter.utils.get_txt_logger(self.model_dir)
        self.csv_file, self.csv_logger = rl_starter.utils.get_csv_logger(self.model_dir)
        self.tb_writer = SummaryWriter(self.model_dir)

        # Log command and all script arguments

        self.txt_logger.info("{}\n".format(" ".join(sys.argv)))
        self.txt_logger.info("{}\n".format(self.args))

    def __set_device(self):
        # Set seed for all randomness sources
        rl_starter.utils.seed(self.args.seed)

        # Set device
        self.device = torch.device("cuda") if not self.args.cpu else torch.device("cpu")

        if self.args.cpu:
            torch.set_num_threads(self.args.number_threads)

        self.txt_logger.info(f"Device: {self.device}\n")

    def __load_env(self):
        self.envs = list()
        for i in range(self.args.procs):
            self.envs.append(rl_starter.utils.make_env(self.args.env, self.args.seed + 10000 * i))

        self.txt_logger.info("Environments loaded\n")

    def __load_training_status(self):
        self.status = None
        try:
            self.status = rl_starter.utils.get_status(self.model_dir)
            self.txt_logger.info("Training status loaded\n")
        except OSError as e:
            self.txt_logger.error(f"Can not get status of {self.model_dir}. \n Error message {e}")

        if self.status is None:
            self.status = {"num_frames": 0, "update": 0}
            self.txt_logger.info("Setting new training model")

    def __load_observation_preprocessor(self):
        self.obs_space, self.preprocess_obss = rl_starter.utils.get_obss_preprocessor(
            self.envs[0].observation_space)

        if "vocab" in self.status:
            self.preprocess_obss.vocab.load_vocab(self.status["vocab"])

        self.txt_logger.info("Observations preprocessor loaded")

    def __load_model(self):
        self.ac_model = ACModel(self.obs_space, self.envs[0].action_space, self.args.mem, self.args.text)

        if "model_state" in self.status:
            self.ac_model.load_state_dict(self.status["model_state"])

        self.ac_model.to(self.device)

        self.txt_logger.info("Model loaded\n")
        self.txt_logger.info("{}\n".format(self.ac_model))

    def __load_algorithm(self):

        if self.args.algo == "a2c":
            self.algo = torch_ac.A2CAlgo(self.envs, self.ac_model, self.device, self.args.frames_per_proc,
                                         self.args.discount,
                                         self.args.lr, self.args.gae_lambda, self.args.entropy_coef,
                                         self.args.value_loss_coef, self.args.max_grad_norm, self.args.recurrence,
                                         self.args.optim_alpha, self.args.optim_eps, self.preprocess_obss)
        elif self.args.algo == "ppo":
            self.algo = torch_ac.PPOAlgo(self.envs, self.ac_model, self.device, self.args.frames_per_proc,
                                         self.args.discount,
                                         self.args.lr, self.args.gae_lambda, self.args.entropy_coef,
                                         self.args.value_loss_coef, self.args.max_grad_norm, self.args.recurrence,
                                         self.args.optim_eps, self.args.clip_eps, self.args.epochs,
                                         self.args.batch_size,
                                         self.preprocess_obss)
        else:
            raise ValueError("Incorrect algorithm name: {}".format(self.args.algo))

        if "optimizer_state" in self.status:
            self.algo.optimizer.load_state_dict(self.status["optimizer_state"])

        self.txt_logger.info("Optimizer loaded\n")

    def __init__(self):
        self.args = train.args.parse_arguments_for_training_one_model()

        self.ac_model = None
        
        self.__get_general_info()
        self.__set_device()
        self.__load_env()
        self.__load_training_status()
        self.__load_observation_preprocessor()
        self.__load_model()
        self.__load_algorithm()

    def __save_status(self, update: int, num_frames: int):
        status = {"num_frames": num_frames, "update": update,
                  "model_state": self.ac_model.state_dict(),
                  "optimizer_state": self.algo.optimizer.state_dict()}

        if hasattr(self.preprocess_obss, "vocab"):
            status["vocab"] = self.preprocess_obss.vocab.vocab

        rl_starter.utils.save_status(status, self.model_dir)
        self.txt_logger.info("Status saved")
        pass

    def train_model(self):

        num_frames = self.status["num_frames"]
        update = self.status["update"]
        start_time = time.time()

        while num_frames < self.args.frames:
            # Update model parameters
            update_start_time = time.time()

            exps, logs1 = self.algo.collect_experiences()
            logs2 = self.algo.update_parameters(exps)

            logs = {**logs1, **logs2}
            update_end_time = time.time()

            num_frames += logs["num_frames"]
            update += 1

            if update % self.args.log_interval == 0:
                rl_starter.utils.print_logs(logs, start_time, update_start_time, update_end_time, update,
                                            num_frames, self.txt_logger, self.status, self.csv_file,
                                            self.csv_logger, self.tb_writer)

            if self.args.save_interval > 0 and update % self.args.save_interval == 0:
                self.__save_status(update, num_frames)
