#!/usr/bin/env python3
# coding --utf-8--

import argparse


def __parse_evaluate_params(parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    # Parameters for main algorithm
    parser.add_argument("--env", required=True,
                        help="name of the environment (REQUIRED)")
    parser.add_argument("--model", required=True,
                        help="name of the trained model (REQUIRED)")
    parser.add_argument("--episodes", type=int, default=100,
                        help="number of episodes of evaluation (default: 100)")
    parser.add_argument("--seed", type=int, default=0,
                        help="random seed (default: 0)")
    parser.add_argument("--procs", type=int, default=16,
                        help="number of processes (default: 16)")
    parser.add_argument("--argmax", action="store_true", default=False,
                        help="action with highest probability is selected")
    parser.add_argument("--worst-episodes-to-show", type=int, default=10,
                        help="how many worst episodes to show")
    parser.add_argument("--memory", action="store_true", default=False,
                        help="add a LSTM to the model")
    parser.add_argument("--text", action="store_true", default=False,
                        help="add a GRU to the model")

    return parser


def parse_arguments_for_evaluate_model():
    """
        Parse all needed helper function for training only one model
    """
    # Parse arguments
    parser = argparse.ArgumentParser()

    # General parameters
    parser = __parse_evaluate_params(parser)

    args = parser.parse_args()

    return args
