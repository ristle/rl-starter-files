#!/usr/bin/env python3
# coding --utf-8--

from torch_ac.utils.penv import ParallelEnv
from rl_starter.scripts import evaluate
from rl_starter.model import ACModel
import rl_starter.utils
import gym_minigrid
import argparse
import torch
import time


class Evaluate(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            print('Creating the object...')
            cls._instance = super(Evaluate, cls).__new__(cls)
            print('Done.')
            # Put any initialization here.
        return cls._instance

    def __set_device(self):
        rl_starter.utils.seed(self.args.seed)

        # Set device
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print(f"Device: {self.device}\n")

    def __load_environment(self):
        self.envs = []

        for i in range(self.args.procs):
            env = rl_starter.utils.make_env(self.args.env, self.args.seed + 10000 * i)
            self.envs.append(env)

        self.env = ParallelEnv(self.envs)
        print("Environments loaded\n")

    def __load_agent(self):
        self.model_dir = rl_starter.utils.get_model_dir(self.args.model)
        print("Agent loaded\n")
        self.agent = rl_starter.utils.Agent(self.env.observation_space, self.env.action_space, self.model_dir,
                                            device=self.device, argmax=self.args.argmax, num_envs=self.args.procs,
                                            use_memory=self.args.memory, use_text=self.args.text)

    def __init__(self):
        self.args = evaluate.args.parse_arguments_for_evaluate_model()

        self.__set_device()
        self.__load_environment()
        self.__load_agent()

        self.log_episode_num_frames = torch.zeros(self.args.procs, device=self.device)

    def __init_logs(self):
        self.logs = {"num_frames_per_episode": [], "return_per_episode": []}
        self.start_time = time.time()

        self.obss = self.env.reset()

        self.log_done_counter = 0
        self.log_episode_return = torch.zeros(self.args.procs, device=self.device)

    def __print_logs(self):
        num_frames = sum(self.logs["num_frames_per_episode"])
        fps = num_frames / (self.end_time - self.start_time)
        duration = int(self.end_time - self.start_time)
        return_per_episode = rl_starter.utils.synthesize(self.logs["return_per_episode"])
        num_frames_per_episode = rl_starter.utils.synthesize(self.logs["num_frames_per_episode"])

        print("F {} | FPS {:.0f} | D {} | R:μσmM {:.2f} {:.2f} {:.2f} {:.2f} | F:μσmM {:.1f} {:.1f} {} {}"
              .format(num_frames, fps, duration,
                      *return_per_episode.values(),
                      *num_frames_per_episode.values()))

    def __evaluate(self):
        while self.log_done_counter < self.args.episodes:
            actions = self.agent.get_actions(self.obss)
            self.obss, rewards, dones, _ = self.env.step(actions)
            self.agent.analyze_feedbacks(rewards, dones)

            self.log_episode_return += torch.tensor(rewards, device=self.device, dtype=torch.float)
            self.log_episode_num_frames += torch.ones(self.args.procs, device=self.device)

            for i, done in enumerate(dones):
                if done:
                    self.log_done_counter += 1
                    self.logs["return_per_episode"].append(self.log_episode_return[i].item())
                    self.logs["num_frames_per_episode"].append(self.log_episode_num_frames[i].item())

            mask = 1 - torch.tensor(dones, device=self.device, dtype=torch.float)
            self.log_episode_return *= mask
            self.log_episode_num_frames *= mask

        self.end_time = time.time()

    def __print_worst_logs(self):
        n = self.args.worst_episodes_to_show
        if n > 0:
            print("\n{} worst episodes:".format(n))
            indexes = sorted(range(len(self.logs["return_per_episode"])),
                             key=lambda k: self.logs["return_per_episode"][k])
            for i in indexes[:n]:
                print("- episode {}: R={}, F={}".format(i, self.logs["return_per_episode"][i],
                                                        self.logs["num_frames_per_episode"][i]))

    def __call__(self, *args, **kwargs):
        self.__init_logs()
        self.__evaluate()
        self.__print_logs()
        self.__print_worst_logs()
