#!/usr/bin/env python

import sys
import time
import torch
import argparse
import datetime
import torch_ac
from torch.utils.tensorboard import SummaryWriter

import rl_starter.utils as utils
from rl_starter.model import ACModel

import gym
# This module needed to import my written classes!
import gym_simple_grid

# TODO: Refactor this shit

if __name__ == '__main__':
    def make_env(env_key, seed=None):
        env = gym.make(env_key)
        env2 = gym.make('ComplexGridWithAIRobots-v2')

        env2.normal_init(env.second_agent_pose, env.action_space, env.grid, env.position, env.target,
                         env.grid_size)
        env2.seed(seed)
        env.seed(seed)
        return env, env2


    # Parse arguments

    parser = argparse.ArgumentParser()

    ## General parameters
    parser.add_argument("--algo", required=False, default='ppo2',
                        help="algorithm to use: a2c | ppo (REQUIRED)")
    parser.add_argument("--env", required=False, default='ComplexGridWithAIRobots-v2',
                        help="name of the environment to train on (REQUIRED)")
    parser.add_argument("--model", required=False, default='ComplexGridWithAIRobots-v2-3-Memory8',
                        help="name of the model (default: {ENV}_{ALGO}_{TIME})")
    parser.add_argument("--seed", type=int, default=1,
                        help="random seed (default: 1)")
    parser.add_argument("--log-interval", type=int, default=1,
                        help="number of updates between two logs (default: 1)")
    parser.add_argument("--save-interval", type=int, default=10,
                        help="number of updates between two saves (default: 10, 0 means no saving)")
    parser.add_argument("--procs", type=int, default=16,
                        help="number of processes (default: 16)")
    parser.add_argument("--frames", type=int, default=10 ** 7,
                        help="number of frames of training (default: 1e7)")

    ## Parameters for main algorithm
    parser.add_argument("--epochs", type=int, default=4,
                        help="number of epochs for PPO (default: 4)")
    parser.add_argument("--batch-size", type=int, default=256,
                        help="batch size for PPO (default: 256)")
    parser.add_argument("--frames-per-proc", type=int, default=None,
                        help="number of frames per process before update (default: 5 for A2C and 128 for PPO)")
    parser.add_argument("--discount", type=float, default=0.99,
                        help="discount factor (default: 0.99)")
    parser.add_argument("--lr", type=float, default=0.001,
                        help="learning rate (default: 0.001)")
    parser.add_argument("--gae-lambda", type=float, default=0.95,
                        help="lambda coefficient in GAE formula (default: 0.95, 1 means no gae)")
    parser.add_argument("--entropy-coef", type=float, default=0.01,
                        help="entropy term coefficient (default: 0.01)")
    parser.add_argument("--value-loss-coef", type=float, default=0.5,
                        help="value loss term coefficient (default: 0.5)")
    parser.add_argument("--max-grad-norm", type=float, default=0.5,
                        help="maximum norm of gradient (default: 0.5)")
    parser.add_argument("--optim-eps", type=float, default=1e-8,
                        help="Adam and RMSprop optimizer epsilon (default: 1e-8)")
    parser.add_argument("--optim-alpha", type=float, default=0.99,
                        help="RMSprop optimizer alpha (default: 0.99)")
    parser.add_argument("--clip-eps", type=float, default=0.2,
                        help="clipping epsilon for PPO (default: 0.2)")
    parser.add_argument("--recurrence", type=int, default=8,
                        help="number of time-steps gradient is backpropagated (default: 1). If > 1, a LSTM is added to the model to have memory.")
    parser.add_argument("--text", action="store_true", default=False,
                        help="add a GRU to the model to handle text input")

    args = parser.parse_args()
    args.mem = args.recurrence > 1

    # Set run dir

    date = datetime.datetime.now().strftime("%y-%m-%d-%H-%M-%S")
    default_model_name = f"{args.env}_{args.algo}_seed{args.seed}"

    model_name = args.model or default_model_name
    model_dir = utils.get_model_dir(model_name)
    model_dir2 = utils.get_model_dir(model_name + "second_agent")

    # Load loggers and Tensorboard writer

    txt_logger = utils.get_txt_logger(model_dir)
    csv_file, csv_logger = utils.get_csv_logger(model_dir)
    tb_writer = SummaryWriter(model_dir)

    txt_logger2 = utils.get_txt_logger(model_dir2)
    csv_file2, csv_logger2 = utils.get_csv_logger(model_dir2)
    tb_writer2 = SummaryWriter(model_dir2)

    # Log command and all script arguments

    txt_logger.info("{}\n".format(" ".join(sys.argv)))
    txt_logger.info("{}\n".format(args))

    txt_logger2.info("{}\n".format(" ".join(sys.argv)))
    txt_logger2.info("{}\n".format(args))
    # Set seed for all randomness sources

    utils.seed(args.seed)

    # Set device

    device = torch.device("cpu")
    device2 = torch.device("cpu")
    txt_logger.info(f"Device 1: {device}\n")
    txt_logger.info(f"Device 2: {device2}\n")
    txt_logger2.info(f"Device 1: {device}\n")
    txt_logger2.info(f"Device 2: {device2}\n")

    # Load environments

    envs = []
    envs2 = []
    for i in range(args.procs):
        e1, e2 = make_env(args.env, args.seed + 10000 * i)
        envs.append(e1)
        envs2.append(e2)
    txt_logger.info("Environments loaded\n")
    txt_logger2.info("Environments loaded\n")

    # Load training status

    try:
        status = utils.get_status(model_dir)
    except OSError:
        pass
    status = {"num_frames": 0, "update": 0}
    status2 = {"num_frames": 0, "update": 0}
    txt_logger.info("Training status loaded\n")
    txt_logger2.info("Training status loaded\n")

    # Load observations preprocessor

    obs_space, preprocess_obss = utils.get_obss_preprocessor(envs[0].observation_space)
    obs_space2, preprocess_obss2 = utils.get_obss_preprocessor(envs2[0].observation_space)
    if "vocab" in status:
        preprocess_obss.vocab.load_vocab(status["vocab"])
        preprocess_obss2.vocab.load_vocab(status["vocab"])
    txt_logger.info("Observations preprocessor loaded")
    txt_logger2.info("Observations preprocessor loaded")

    # Load model
    acmodel = ACModel(obs_space, envs[0].action_space, args.mem, args.text)
    acmodel2 = ACModel(obs_space2, envs2[0].action_space, args.mem, args.text)

    if "model_state" in status:
        acmodel.load_state_dict(status["model_state"])
    acmodel.to(device)
    acmodel2.to(device2)
    txt_logger.info("Model loaded\n")
    txt_logger.info("{}\n".format(acmodel))
    txt_logger.info("Model loaded 2\n")
    txt_logger.info("{}\n".format(acmodel2))

    txt_logger2.info("Model loaded\n")
    txt_logger2.info("{}\n".format(acmodel))
    txt_logger2.info("Model loaded 2\n")
    txt_logger2.info("{}\n".format(acmodel2))
    # Load algo
    if args.algo == "a2c":
        algo = torch_ac.A2CAlgo(envs, acmodel, device, args.frames_per_proc, args.discount, args.lr, args.gae_lambda,
                                args.entropy_coef, args.value_loss_coef, args.max_grad_norm, args.recurrence,
                                args.optim_alpha, args.optim_eps, preprocess_obss)
    elif args.algo == "ppo":
        algo = torch_ac.PPOAlgo(envs, acmodel, device, args.frames_per_proc, args.discount, args.lr, args.gae_lambda,
                                args.entropy_coef, args.value_loss_coef, args.max_grad_norm, args.recurrence,
                                args.optim_eps, args.clip_eps, args.epochs, args.batch_size, preprocess_obss)
    elif args.algo == "ppo2":
        algo = torch_ac.PPOAlgo2MOdels(envs, envs2, acmodel, acmodel2, device, device2, args.frames_per_proc,
                                       args.discount,
                                       args.lr, args.gae_lambda,
                                       args.entropy_coef, args.value_loss_coef, args.max_grad_norm, args.recurrence,
                                       args.optim_eps, args.clip_eps, args.epochs, args.batch_size, preprocess_obss,
                                       preprocess_obss2)
    else:
        raise ValueError("Incorrect algorithm name: {}".format(args.algo))

    if "optimizer_state" in status:
        algo.optimizer.load_state_dict(status["optimizer_state"])
        if args.algo == "ppo2":
            algo.optimizer2.load_state_dict(status["optimizer_state"])
    txt_logger.info("Optimizer loaded\n")
    txt_logger2.info("Optimizer loaded\n")

    # Train model

    num_frames = status["num_frames"]
    update = status["update"]
    start_time = time.time()

    while num_frames < args.frames:
        # Update model parameters

        update_start_time = time.time()
        exps, logs1, _exps, _logs1 = algo.collect_experiences()
        if exps is None:
            continue
        if logs1 is None:
            continue
        logs2 = algo.update_parameters(exps)
        logs = {**logs1, **logs2}

        _logs2 = algo.update_parameters(_exps, True)
        _logs = {**_logs1, **_logs2}
        update_end_time = time.time()

        num_frames += logs["num_frames"]
        update += 1

        # Print logs
        if update % args.log_interval == 0:
            fps = logs["num_frames"] / (update_end_time - update_start_time)
            duration = int(time.time() - start_time)
            return_per_episode = utils.synthesize(logs["return_per_episode"])
            rreturn_per_episode = utils.synthesize(logs["reshaped_return_per_episode"])
            num_frames_per_episode = utils.synthesize(logs["num_frames_per_episode"])

            header = ["update", "frames", "FPS", "duration"]
            data = [update, num_frames, fps, duration]
            header += ["rreturn_" + key for key in rreturn_per_episode.keys()]
            data += rreturn_per_episode.values()
            header += ["num_frames_" + key for key in num_frames_per_episode.keys()]
            data += num_frames_per_episode.values()
            header += ["entropy", "value", "policy_loss", "value_loss", "grad_norm"]
            data += [logs["entropy"], logs["value"], logs["policy_loss"], logs["value_loss"], logs["grad_norm"]]

            txt_logger.info(
                "FIRST_MODEL  | U {} | F {:06} | FPS {:04.0f} | D {} | rR:μσmM {:.2f} {:.2f} {:.2f} {:.2f} | "
                "F:μσmM {:.1f} {:.1f} {} {} | H {:.3f} | V {:.3f} | pL {:.3f} | vL {:.3f} | ∇ {:.3f}"
                    .format(*data))

            header += ["return_" + key for key in return_per_episode.keys()]
            data += return_per_episode.values()

            if status["num_frames"] == 0:
                csv_logger.writerow(header)
            csv_logger.writerow(data)
            csv_file.flush()

            for field, value in zip(header, data):
                tb_writer.add_scalar(field, value, num_frames)

            # FUCK IT Second model

            fps2 = _logs["num_frames"] / (update_end_time - update_start_time)
            duration = int(time.time() - start_time)
            return_per_episode2 = utils.synthesize(_logs["return_per_episode"])
            rreturn_per_episode2 = utils.synthesize(_logs["reshaped_return_per_episode"])
            num_frames_per_episode2 = utils.synthesize(_logs["num_frames_per_episode"])

            header2 = ["update", "frames", "FPS", "duration"]
            _data = [update, num_frames, fps2, duration]
            header2 += ["rreturn_" + key for key in rreturn_per_episode2.keys()]
            _data += rreturn_per_episode2.values()
            header2 += ["num_frames_" + key for key in num_frames_per_episode2.keys()]
            _data += num_frames_per_episode2.values()
            header2 += ["entropy", "value", "policy_loss", "value_loss", "grad_norm"]
            _data += [_logs["entropy"], _logs["value"], _logs["policy_loss"], _logs["value_loss"], _logs["grad_norm"]]

            txt_logger2.info(
                "SECOND_MODEL | U {} | F {:06} | FPS {:04.0f} | D {} | rR:μσmM {:.2f} {:.2f} {:.2f} {:.2f} | "
                "F:μσmM {:.1f} {:.1f} {} {} | H {:.3f} | V {:.3f} | pL {:.3f} | vL {:.3f} | ∇ {:.3f}"
                    .format(*_data))

            header2 += ["return_" + key for key in return_per_episode2.keys()]
            _data += return_per_episode2.values()

            if status2["num_frames"] == 0:
                csv_logger2.writerow(header2)
            csv_logger2.writerow(_data)
            csv_file2.flush()

            for field, value in zip(header, _data):
                tb_writer.add_scalar(field, value, num_frames)

        # Save status

        if args.save_interval > 0 and update % args.save_interval == 0:
            status = {"num_frames": num_frames, "update": update,
                      "model_state": acmodel.state_dict(), "optimizer_state": algo.optimizer.state_dict()}
            if hasattr(preprocess_obss, "vocab"):
                status["vocab"] = preprocess_obss.vocab.vocab
            utils.save_status(status, model_dir)

            txt_logger.info("Status saved")
            # SECOND MODEL
            status2 = {"num_frames": num_frames, "update": update,
                       "model_state": acmodel2.state_dict(), "optimizer_state": algo.optimizer2.state_dict()}
            if hasattr(preprocess_obss2, "vocab"):
                status2["vocab"] = preprocess_obss2.vocab.vocab
            utils.save_status(status2, model_dir2)
            txt_logger2.info("Status saved")
