#!/usr/bin/env python
# coding --utf-8--

from torch.utils.tensorboard import SummaryWriter
import rl_starter
import logging
import typing
import time
import csv


def print_logs(logs: dict, start_time: float, update_start_time: float, update_end_time: float,
               update: int, num_frames: int, txt_logger: logging.Logger, status: dict, csv_file: typing.IO,
               csv_logger: csv.DictWriter, tb_writer: SummaryWriter):
    fps = logs["num_frames"] / (update_end_time - update_start_time)
    duration = int(time.time() - start_time)
    return_per_episode = rl_starter.utils.synthesize(logs["return_per_episode"])
    rreturn_per_episode = rl_starter.utils.synthesize(logs["reshaped_return_per_episode"])
    num_frames_per_episode = rl_starter.utils.synthesize(logs["num_frames_per_episode"])

    header = ["update", "frames", "FPS", "duration"]
    data = [update, num_frames, fps, duration]
    header += ["rreturn_" + key for key in rreturn_per_episode.keys()]
    data += rreturn_per_episode.values()
    header += ["num_frames_" + key for key in num_frames_per_episode.keys()]
    data += num_frames_per_episode.values()
    header += ["entropy", "value", "policy_loss", "value_loss", "grad_norm"]
    data += [logs["entropy"], logs["value"], logs["policy_loss"], logs["value_loss"], logs["grad_norm"]]

    txt_logger.info(
        "U {} | F {:06} | FPS {:04.0f} | D {} | rR:μσmM {:.2f} {:.2f} {:.2f} {:.2f} | "
        "F:μσmM {:.1f} {:.1f} {} {} | H {:.3f} | V {:.3f} | pL {:.3f} | vL {:.3f} | ∇ {:.3f}".format(*data))

    header += ["return_" + key for key in return_per_episode.keys()]
    data += return_per_episode.values()

    if status["num_frames"] == 0:
        csv_logger.writerow(header)
    csv_logger.writerow(data)
    csv_file.flush()

    for field, value in zip(header, data):
        tb_writer.add_scalar(field, value, num_frames)
